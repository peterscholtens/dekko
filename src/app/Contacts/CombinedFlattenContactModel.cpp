/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CombinedFlattenContactModel.h"
#include <QDebug>
#include <cassert>

namespace Dekko {
namespace Contacts {

CombinedFlattenContactModel::CombinedFlattenContactModel(QObject *parent)
    : RowsJoinerProxy(parent), m_savedContactModel(0), m_recentContactModel(0)
{
}

QHash<int, QByteArray> CombinedFlattenContactModel::roleNames() const
{
    if (m_savedContactModel)
        return m_savedContactModel->roleNames();
    else if (m_recentContactModel)
        return m_recentContactModel->roleNames();
    else
        assert(false);
}

QAbstractItemModel *CombinedFlattenContactModel::savedContactModel()
{
    return m_savedContactModel;
}

void CombinedFlattenContactModel::setSavedContactModel(QAbstractItemModel *savedContactModel)
{
    m_savedContactModel = savedContactModel;
    insertSourceModel(savedContactModel, 0);
}

QAbstractItemModel *CombinedFlattenContactModel::recentContactModel()
{
    return m_recentContactModel;
}

void CombinedFlattenContactModel::setRecentContactModel(QAbstractItemModel *recentContactModel)
{
    m_recentContactModel = recentContactModel;
    insertSourceModel(recentContactModel);
}

}
}
