#include "DekkoGlobal.h"
#include <QtQuick>
#include <QQmlContext>
#include <QtQml/QtQml>
#include "Common/Application.h"
#include "Common/MetaTypes.h"
#include "Common/SettingsNames.h"
#include "app/Composition/ReplyMode.h"
#include "app/Components/ListViewWithPageHeader.h"
#include "app/Components/QmlObjectListModel.h"
#include "app/Components/ImageHelper.h"
#include "app/Components/qlimitproxymodelqml.h"
#include "app/Contacts/ContactsModel.h"
#include "app/Contacts/ContactFilterModel.h"
#include "app/Contacts/FlattenContactModel.h"
#include "app/Contacts/CombinedFlattenContactModel.h"
#include "app/Contacts/CombinedFlattenContactFilterModel.h"
#include "app/Accounts/Account.h"
#include "app/Accounts/AccountsManager.h"
#include "app/Accounts/PasswordManager.h"
#include "app/Models/AccountsListModel.h"
#include "app/Accounts/SenderIdentity.h"
#include "app/Accounts/SenderIdentityModel.h"
#include "app/Accounts/MultipleAccountSenderIdentityModel.h"
#include "app/Composition/SubmissionManager.h"
#include "app/Composition/ComposerUtils.h"
#include "app/MailConfig/AutoConfig.h"
#include "app/MailConfig/MailConfig.h"
#include "app/MailConfig/ImapSettingVerifier.h"
#include "app/Models/Recipient.h"
#include "app/Models/RecipientListModel.h"
#include "app/Models/RecipientListFilterModel.h"
#include "app/Models/MessageListModel.h"
#include "app/Models/MessageListFilterModel.h"
#include "app/Models/MessageModel.h"
#include "app/Models/PreSetProviderModel.h"
#include "app/Models/MailboxProxyModel.h"
#include "app/Models/MailboxFilterModel.h"
#include "app/Settings/Settings.h"
#include "app/Settings/GlobalSettings.h"
#include "app/Settings/ViewSettings.h"
#include "app/Utils/AttachmentDownloader.h"
#include "app/Utils/Path.h"
#include "app/Utils/PlainTextFormatter.h"
#include "app/Utils/TabEventSignaler.h"
#include "app/Models/MessageBuilder.h"
#include "app/Utils/MailboxSearch.h"
#include "app/Utils/MailboxUtils.h"
#include "app/Utils/UidIterator.h"
#include "app/Utils/SearchForSpecialMailbox.h"
#include "app/Theme/Theme.h"
#include "app/Network/MsgPartNetAccessManagerFactory.h"
#include "Imap/Model/ThreadingMsgListModel.h"
#include "Imap/Model/kdeui-itemviews/kdescendantsproxymodel.h"
#include "app/Settings/MailboxSettings.h"
#include "app/Settings/SettingsFileBase.h"
#include "app/Settings/NotificationSettings.h"
#include "app/Notifications/NotificationService.h"
#include "app/Notifications/PostalServiceWorker.h"


void DekkoGlobal::registerDekkoTypes(const char *uri, int major, int minor)
{
    // @uri DekkoCore
    Common::registerMetaTypes();
    qmlRegisterType<Dekko::Notifications::NotificationService>(uri, major, minor, "NotificationService");
    qmlRegisterType<Dekko::Components::ListViewWithPageHeader>(uri, major, minor, "ListViewWithPageHeader");
    qmlRegisterType<QLimitProxyModelQML>(uri, major, minor, "LimitProxyModel");
    qmlRegisterType<Dekko::Components::ImageHelper>(uri, major, minor, "ImageHelper");
    qmlRegisterType<Dekko::AutoConfig>(uri, major, minor, "AutoConfig");
    qmlRegisterType<Dekko::ImapSettingVerifier>(uri, major, minor, "ImapSettingVerifier");
    qmlRegisterType<Dekko::ReplyMode>(uri, major, minor, "ReplyMode");
    qmlRegisterType<Dekko::ComposerUtils>(uri, major, minor, "ComposerUtils");
    qmlRegisterType<Dekko::Models::RecipientListFilterModel>(uri, major, minor, "RecipientListFilterModel");
    qmlRegisterType<Dekko::SubmissionManager>(uri, major, minor, "SubmissionManager");
    qmlRegisterType<Dekko::Models::Recipient>(uri, major, minor, "Recipient");
    qmlRegisterType<Dekko::Models::RecipientListModel>(uri, major, minor, "RecipientListModel");
    qmlRegisterType<Dekko::Accounts::Account>(uri, major, minor, "Account");
    qmlRegisterType<Dekko::Accounts::AccountsManager>(uri, major, minor, "AccountsManager");
    qmlRegisterType<Dekko::Accounts::PasswordManager>(uri, major, minor, "PasswordManager");
    qmlRegisterType<Dekko::Models::AccountsListModel>(uri, major, minor, "AccountsListModel");
    qmlRegisterType<Dekko::Accounts::SenderIdentity>(uri, major, minor, "SenderIdentity");
    qmlRegisterType<Dekko::Accounts::SenderIdentityModel>(uri, major, minor, "SenderIdentityModel");
    qmlRegisterType<Dekko::Accounts::MultipleAccountSenderIdentityModel>(uri, major, minor, "MultipleAccountSenderIdentityModel");
    qmlRegisterType<Dekko::Models::RecipientListModel>(uri, major, minor, "RecipientListModel");
    qmlRegisterType<Dekko::Models::MessageListModel>(uri, major, minor, "MessageListModel");
    qmlRegisterType<Dekko::Models::MessageListFilterModel>(uri, major, minor, "MessageListFilterModel");
    qmlRegisterType<Dekko::Models::MessageModel>(uri, major, minor, "MessageModel");
    qmlRegisterType<Dekko::Utils::AttachmentDownloader>(uri, major, minor, "AttachmentDownloader");
    qmlRegisterType<Dekko::Utils::PlainTextFormatter>(uri, major, minor, "PlainTextFormatter");
    qmlRegisterType<Dekko::Utils::MessageBuilder>(uri, major, minor, "MessageBuilder");
    qmlRegisterType<Dekko::Utils::MailboxSearch>(uri, major, minor, "MailboxSearch");
    qmlRegisterType<Dekko::Utils::MailboxUtils>(uri, major, minor, "MailboxUtils");
    qmlRegisterType<Dekko::Utils::UidIterator>(uri, major, minor, "UidIterator");
    qmlRegisterType<Dekko::Utils::SearchForSpecialMailbox>(uri, major, minor, "SearchForSpecialMailbox");
    qmlRegisterType<Dekko::Settings::ImapSettings>(uri, major, minor, "ImapSettings");
    qmlRegisterType<Dekko::Settings::SmtpSettings>(uri, major, minor, "SmtpSettings");
    qmlRegisterType<Dekko::Settings::AccountProfile>(uri, major, minor, "AccountProfile");
    qmlRegisterType<Dekko::Settings::Preferences>(uri, major, minor, "Preferences");
    qmlRegisterType<Dekko::Settings::OfflineSettings>(uri, major, minor, "OfflineSettings");
    qmlRegisterType<Dekko::Settings::DeveloperSettings>(uri, major, minor, "DeveloperSettings");
    qmlRegisterType<Dekko::Settings::UOASettings>(uri, major, minor, "UOASettings");
    qmlRegisterType<Dekko::Settings::MailboxSettings>(uri, major, minor, "MailboxSettings");
    qmlRegisterType<Dekko::Settings::NotificationSettings>(uri, major, minor, "NotificationSettings");
    qmlRegisterType<Dekko::Settings::ViewSettings>(uri, major, minor, "ViewSettings");
    qmlRegisterType<Dekko::Models::PreSetProviderModel>(uri, major, minor, "PreSetProvidersModel");
    qmlRegisterType<Dekko::Models::MailboxProxyModel>(uri, major, minor, "MailboxProxyModel");
    qmlRegisterType<Dekko::Models::MailboxFilterModel>(uri, major, minor, "MailboxFilterModel");
    qmlRegisterType<Dekko::Contacts::ContactsModel>(uri, major, minor, "DekkoContactsModel");
    qmlRegisterType<Dekko::Contacts::ContactFilterModel>(uri, major, minor, "ContactFilterModel");
    qmlRegisterType<Dekko::Contacts::FlattenContactModel>(uri, major, minor, "FlattenContactModel");
    qmlRegisterType<Dekko::Contacts::CombinedFlattenContactFilterModel>(uri, major, minor, "CombinedFlattenContactFilterModel");
    qmlRegisterType<Dekko::Contacts::CombinedFlattenContactModel>(uri, major, minor, "CombinedFlattenContactModel");
    qmlRegisterType<KDescendantsProxyModel>(uri, major, minor, "FlatteningProxyModel");
    qmlRegisterType<Imap::Network::MsgPartNetAccessManager>(uri, major, minor, "MsgPartNetAccessManager");
    qmlRegisterUncreatableType<Dekko::Components::QmlObjectListModel>(uri, major, minor, "QmlObjectListModel", QLatin1String("Can't create an instance from qml"));
    qmlRegisterUncreatableType<Dekko::ComposerUtils>(uri, major, minor, "RecipientKind",
                                                            QLatin1String("Can't create recipientkind from qml"));
    qmlRegisterSingletonType<Dekko::Utils::TabEventSignaler>(uri, major, minor, "TabEventSignaler", Dekko::Utils::TabEventSignaler::factory);
    qmlRegisterSingletonType<Dekko::Settings::GlobalSettings>(uri, major, minor, "GlobalSettings", Dekko::Settings::GlobalSettings::factory);
    qmlRegisterSingletonType<Dekko::Themes::Theme>(uri, major, minor, "Style", Dekko::Themes::Theme::factory);
}

void DekkoGlobal::registerTrojitaCoreTypes(const char *uri, int major, int minor)
{
    // @uri TrojitaCore
    qmlRegisterUncreatableType<Imap::Mailbox::ThreadingMsgListModel>(uri, major, minor, "ThreadingMsgListModel",
            QLatin1String("ThreadingMsgListModel can only be created from the C++ code.\
                          Use ImapAccess if you need access to an instance."));
    qmlRegisterSingletonType<UiUtils::Formatting>(uri, major, minor, "UiFormatting", UiUtils::Formatting::factory);
    qmlRegisterUncreatableType<Composer::Submission>(uri, major, minor, "SubmissionMethod",
            QLatin1String("Composer::Submission::Method can be only created from the C++ code."));
    qmlRegisterUncreatableType<UiUtils::PasswordWatcher>(uri, major, minor, "PasswordWatcher",
            QLatin1String("PasswordWatcher can only be created from the C++ code. Use ImapAccess\
                          if you need access to an instance."));
}
