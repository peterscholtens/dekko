/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef VIEWSETTINGS_H
#define VIEWSETTINGS_H
#include "SettingsObjectBase.h"
#include <QVariantMap>
#include "SettingsFileBase.h"

namespace Dekko
{
namespace Settings
{

// View settings will just use the accountId as the base path
// So the path property needs to be set to Account::accountId.
class ViewSettings : public SettingsObjectBase
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap keys READ keys CONSTANT)
public:
    explicit ViewSettings(QObject *parent = 0);

    const QVariantMap keys();
    static SettingsFileBase *viewSettingsFile();
    static void setViewSettingsFile(SettingsFileBase *file);

signals:
    void objectDataChanged();

private:

    friend class TestViewSettings;
};
}
}
#endif // VIEWSETTINGS_H
