/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ViewSettings.h"
#include <QPointer>

namespace Dekko
{
namespace Settings
{
ViewSettings::ViewSettings(QObject *parent) :
    SettingsObjectBase(parent)
{
    setFilePath(viewSettingsFile());
    if (m_file) {
        // initially set the path to the root object
        setPath(QString(""));
//        write(QLatin1String("Works"), true);
    }
    connect(this, SIGNAL(dataChanged()), this, SIGNAL(objectDataChanged()));
}

const QVariantMap ViewSettings::keys()
{
    QVariantMap keys;
    keys["currentMailbox"] = QLatin1String("current_mailbox");
    keys["selectedUid"] = QLatin1String("selected_uid");
    return keys;
}
static QPointer<SettingsFileBase> vSettingsFile;
SettingsFileBase *ViewSettings::viewSettingsFile()
{
    return vSettingsFile;
}

void ViewSettings::setViewSettingsFile(SettingsFileBase *file)
{
    vSettingsFile = file;
}
}
}
