#ifndef DEKKOGLOBAL_H
#define DEKKOGLOBAL_H
#include <QtGlobal>

class DekkoGlobal
{
public:
    static void registerDekkoTypes(const char* uri, int major = 0, int minor = 2);

    static void registerTrojitaCoreTypes(const char *uri, int major = 0, int minor = 1);
};

#endif // DEKKOGLOBAL_H
