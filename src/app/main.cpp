/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko mail client

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3 or any later version
   accepted by the membership of KDE e.V. (or its successor approved
   by the membership of KDE e.V.), which shall act as a proxy
   defined in Section 14 of version 3 of the license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// NOTE: uncomment to enable profiling
//#define QML_COMPILER_STATS
//#define QT_QML_DEBUG
#include <QtQuick>
#include <QDebug>
#include <QQmlContext>
#include <QSettings>
#include <QtGui/QGuiApplication>
#include <QtQml/QtQml>
#include <QtQuick/QQuickView>
#include <QTranslator>
#include <QLocale>
#include <QScopedPointer>
#include "AppVersion/SetCoreApplication.h"
#include "Common/Application.h"
#include "Common/MetaTypes.h"
#include "Common/SettingsNames.h"
#include "static_plugins.h"
#include "DekkoGlobal.h"
#include "app/Settings/SettingsFileBase.h"
#include "app/Settings/MailboxSettings.h"
#include "app/Settings/NotificationSettings.h"
#include "app/Settings/ViewSettings.h"
#include "app/Utils/Path.h"
#include "app/Network/MsgPartNetAccessManagerFactory.h"
#include "configure.cmake.h"

QQuickView *qQuickViewer;


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    Q_INIT_RESOURCE(actionicons);
    Q_INIT_RESOURCE(providericons);

    // Configure mbox settings
    QScopedPointer<Dekko::Settings::SettingsFileBase> mboxSettings(new Dekko::Settings::SettingsFileBase);
    Dekko::Settings::MailboxSettings::setDefaultFile(mboxSettings.data());
    mboxSettings.data()->setPath(Dekko::configLocation(QStringLiteral("mailboxConfig.json")));
    // Notification settings
    QScopedPointer<Dekko::Settings::SettingsFileBase> notificationSettings(new Dekko::Settings::SettingsFileBase);
    Dekko::Settings::NotificationSettings::setSettingsFile(notificationSettings.data());
    notificationSettings.data()->setPath(Dekko::configLocation(QStringLiteral("notifications.json")));
    // View settings
    QScopedPointer<Dekko::Settings::SettingsFileBase> viewSettings(new Dekko::Settings::SettingsFileBase);
    Dekko::Settings::ViewSettings::setViewSettingsFile(viewSettings.data());
    viewSettings.data()->setPath(Dekko::configLocation(QStringLiteral("viewSettings.json")));

    //Install translations
    QTranslator dekkoTranslater;
    QTranslator dekkoBuildTranslater;
    // It would appear Qt has a real problem loading translations for anywhere but in the same directory
    // as the executable. I have no idea if this is click package related or a bug in Qt but this works
    // so i'm not going to complain.... (*much*)
    if (dekkoTranslater.load(QLocale::system().name(), app.applicationDirPath())) {
        qDebug() << "Translation loaded" ;
        app.installTranslator(&dekkoTranslater);
    } else if (dekkoTranslater.load(QLocale::system().name(), QCoreApplication::applicationDirPath() + QLatin1String("/locale/"))) {
        qDebug() << "Installing translations in build dir";
        app.installTranslator(&dekkoBuildTranslater);
    }


    qQuickViewer = new QQuickView();
    qQuickViewer->setResizeMode(QQuickView::SizeRootObjectToView);
    app.addLibraryPath(QCoreApplication::applicationDirPath());

    Common::registerMetaTypes();
#ifdef CLICK_MODE
    Common::Application::name = QString::fromLatin1("dekko.dekkoproject");
    // If the organization is set, QStandardPaths::DataLocation will be
    // $PREFIX/$APPNAME/$ORGANIZATION e.g.:
    // /usr/share/dekko.dekkoproject/dekko.dekkoproject
    Common::Application::organization = QString::fromLatin1("dekko.dekkoproject");
#else
    Common::Application::name = QString::fromLatin1("dekko.dekkoproject");
    // If the organization is set, QStandardPaths::DataLocation will be
    // $PREFIX/$APPNAME/$ORGANIZATION e.g.:
    // /usr/share/dekko.dekkoproject/dekko.dekkoproject
    Common::Application::organization = QString::fromLatin1("");
#endif

    AppVersion::setVersion();
    AppVersion::setCoreApplicationData();

    // let's first check all standard locations
    QString qmlFile = Dekko::findQmlFile(QString::fromLatin1("qml/main.qml"));

    QUrl addressbookStoreLocation(QString::fromLatin1(
            "file://%1/.local/share/dekko.dekkoproject/addressbook.vcard")
            .arg(QDir::homePath()));
    qQuickViewer->engine()->rootContext()->setContextProperty(QLatin1String("addressbookStoreLocation"), QVariant::fromValue(addressbookStoreLocation));

    Dekko::MsgPartNetAccessManagerFactory msgPartNetAccessManagerFactory;
    qQuickViewer->engine()->setNetworkAccessManagerFactory(&msgPartNetAccessManagerFactory);
    qQuickViewer->engine()->rootContext()->setContextProperty(QLatin1String("msgPartNetAccessManagerFactory"), &msgPartNetAccessManagerFactory);

    qQuickViewer->engine()->rootContext()->setContextProperty(QLatin1String("appUris"), QString::fromLatin1(qgetenv("APP_URIS")));

    DekkoGlobal::registerDekkoTypes("DekkoCore");
    DekkoGlobal::registerTrojitaCoreTypes("TrojitaCore");


    qQuickViewer->setTitle(QObject::trUtf8("Dekko"));
    qQuickViewer->setSource(QUrl::fromLocalFile(qmlFile));
    qQuickViewer->show();
    return app.exec();
}
