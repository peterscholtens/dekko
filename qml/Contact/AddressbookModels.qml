/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import DekkoCore 0.2

// All models needed by addressbook operations are initialized here
Item {
    property alias addressbookModel: addressbookModel
    property alias addressbookModelFiltered: addressbookModelFiltered
    property alias recentContactModel: recentContactModel
    property alias recentContactModelFiltered: recentContactModelFiltered
    property alias combinedFlattenContactFilterModel: combinedFlattenContactFilterModel
    property alias flattenContactModel: flattenContactModel
    property alias flattenRecentContactModel: flattenRecentContactModel
    property alias combinedFlattenContactModel: combinedFlattenContactModel
    AddressbookModel {
        id: addressbookModel
        databaseName: "addressbook"
    }

    ContactFilterModel {
        id: addressbookModelFiltered
        sourceModel: addressbookModel
        sortRole: DekkoContactsModel.RoleTag
    }

    RecentContactModel {
        id: recentContactModel
        databaseName: "recentcontact"
    }

    ContactFilterModel {
        id: recentContactModelFiltered
        sourceModel: recentContactModel
        sortRole: DekkoContactsModel.RoleLastContactDate
    }

    CombinedFlattenContactFilterModel {
        id: combinedFlattenContactFilterModel
        source: combinedFlattenContactModel
    }

    FlattenContactModel{
        id: flattenContactModel
        model: addressbookModelFiltered
    }

    FlattenContactModel{
        id: flattenRecentContactModel
        model: recentContactModelFiltered
    }

    CombinedFlattenContactModel {
        id: combinedFlattenContactModel
        savedContactModel: flattenContactModel
        recentContactModel: flattenRecentContactModel
    }
}
