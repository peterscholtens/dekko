/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import Ubuntu.Components 1.1

ContactDetailPageBase {
    id: root
    contactModel: recentContactModel
    state: "recent"
    property bool contactSaved: addressbookModel.getContactIdWithEmail(contact.emails[0]).length > 0

    secondaryActions: contactSaved ? [] : saveToAddressbookAction

    // Save email collected from recent recipients, to addressbook
    Action {
        id: saveToAddressbookAction
        iconName: "contact-new"
        onTriggered: {
            pageStack.push(Qt.resolvedUrl("./ContactEditPage.qml"), {
                                contact: root.contact,
                                saveFromRecentContact: true
                           }).contactCreated.connect(function() {
                                root.contactSaved = true;
                           })
        }
    }
}
