/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.3
import DekkoCore 0.2

DekkoContactsModel {

    // number of recently contacted contact to remember
    property int maxCount: 50;

    function saveJustContacted (name, email) {
        var contactId = getContactIdWithEmail(email);
        if (contactId !== "") {
            setLastContactTime(contactId, new Date());
        } else {
            contactId = newContact(name, "", "", [email]);
            setLastContactTime(contactId, new Date());
        }
    }

    Component.onCompleted: {
        removeStaleContactMoreThan(maxCount);
    }
}
