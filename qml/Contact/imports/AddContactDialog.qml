/* Copyright (C) 2015 Boren Zhang <bobo1993324@gmail.com>

   Modified based on telegrame app <root>/ui/ContactsPage.qml https://code.launchpad.net/~libqtelegram-team/libqtelegram/telegram-app

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 0.1

Dialog {
     id: root
     title: i18n.tr("Add Contact")
     signal fromAddressbookClicked
     signal enterDetailClicked
     Button {
         text: i18n.tr("From Address Book")
         color: UbuntuColors.orange
         onClicked: {
             PopupUtils.close(root)
             fromAddressbookClicked();
         }
     }
     Button {
         text: i18n.tr("Enter Details")
         color: UbuntuColors.orange
         onClicked: {
             PopupUtils.close(root)
             enterDetailClicked();
         }
     }
     Button {
         text: i18n.tr("Cancel")
         onClicked: PopupUtils.close(root)
     }
 }
