/*
 * Copyright (C) 2012-2014 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.2
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 1.0 as Popups
import Ubuntu.Content 0.1 as ContentHub
import QtContacts 5.0

Item {
    id: root

    property var importDialog: null

    function requestContact()
    {
        if (!root.importDialog) {
            root.importDialog = PopupUtils.open(contentHubDialog, root)
        } else {
            console.warn("Import dialog already running")
        }
    }

    Component {
        id: contentHubDialog

        Popups.PopupBase {
            id: dialogue

            property alias activeTransfer: signalConnections.target
            focus: true

            Rectangle {
                anchors.fill: parent

                ContentHub.ContentPeerPicker {
                    id: peerPicker

                    anchors.fill: parent
                    contentType: ContentHub.ContentType.Contacts
                    handler: ContentHub.ContentHandler.Source

                    onPeerSelected: {
                        peer.selectionType = ContentHub.ContentTransfer.Single
                        dialogue.activeTransfer = peer.request()
                    }

                    onCancelPressed: {
                        PopupUtils.close(root.importDialog)
                    }
                }
            }

            Connections {
                id: signalConnections

                onStateChanged: {
                    if (dialogue.activeTransfer.state === ContentHub.ContentTransfer.Charged) {
                        dialogue.hide()
                        if (dialogue.activeTransfer.items.length > 0) {
                            vCardParser.vCardUrl = dialogue.activeTransfer.items[0].url
                        }
                    }
                }
            }
        }
    }
    VCardParser {
        id: vCardParser
        function parseContact(vcardContact) {
            var contact = {};
            contact.firstName = vcardContact.name.firstName;
            contact.lastName = vcardContact.name.lastName;
            if (contact.firstName === "") {
                var labelName = vcardContact.displayLabel.label.split(" ");
                firstName = labelName[0];
                if (labelName.length >1) {
                    //removes the first name
                    labelName.shift();
                    contact.lastName = labelName.toString().replace(",","");
                } else {
                    contact.lastName = "";
                }
            }
            contact.emails = [];
            for (var i = 0;i < vcardContact.emails.length; i++) {
                if (vcardContact.emails[i].emailAddress.length > 0)
                    contact.emails.push(vcardContact.emails[i].emailAddress);
            }
            return contact;
        }

        onVcardParsed: {
            if (contacts.length === 0) {
                return;
            }
            console.log("Parsed " + contacts.length + " contacts.");
            var contactList = [];
            for (var i = 0; i < contacts.length; i++) {
                var contact = parseContact(contacts[i]);
                if (contact.emails.length > 0 && contact.firstName !== "") {
                    contactList.push(contact);
                }
            }
            addressbookModel.importContacts(contactList)
        }
    }
}
