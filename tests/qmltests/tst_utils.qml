/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import QtTest 1.0
import "../../qml/Utils/Utils.js" as Utils

TestCase {
    name: "UtilsTests"


    function test_mailaddress_valid_data() {
        return [
                    {tag: "validstring", a: "example@example.org", answer: true },
                    {tag: "emptystring", a: "", answer: false },
                    {tag: "undefinedstring", a: undefined, answer: false},
                ]
    }

    function test_address_from_pretty_string_data() {
        return [
                    {tag: "full address", addr: "Foo Bar <foo@bar.com>", result: "foo@bar.com"},
                    {tag: "just address", addr: "foo@bar.com", result: "foo@bar.com"}
                ]
    }

    function test_name_from_pretty_string_data() {
        return [
                    {tag: "With Name", addr: "Foo Bar <foo@bar.com>", result: "Foo Bar"},
                    {tag: "just address", addr: "foo@bar.com", result: "foo@bar.com"}
                ]
    }

    function test_format_plain_address_data() {
        return [
                    {
                        tag: "Full address",
                        array: ["Foo Bar", "", "foobar", "example.com"],
                        complete: "Foo Bar <foobar@example.com>",
                        justName: "Foo Bar", justAddress: "foobar@example.com"
                    },
                    {
                        tag: "partial address",
                        array: ["", "", "foobar", "example.com"],
                        complete: "foobar@example.com",
                        justName: "foobar@example.com", justAddress: "foobar@example.com"
                    }
                ]
    }

    function test_mailaddress_valid(data) {
        compare(Utils.isMailAddressValid(data.a), data.answer, data.tag)
    }

    function test_address_from_pretty_string(data) {
        compare(Utils.emailAddressFromPrettyAddress(data.addr), data.result, data.tag)
    }

    function test_name_from_pretty_string(data) {
        compare(Utils.abbreviateNameForPrettyAddress(data.addr), data.result, data.tag)
    }

    function test_format_plain_address(data) {
        compare(Utils.formatPlainArrayMailAddress(data.array, false, false), data.complete, "Complete: " + data.tag)
        compare(Utils.formatPlainArrayMailAddress(data.array, true, false), data.justName, "Just Name: " + data.tag)
        compare(Utils.formatPlainArrayMailAddress(data.array, false, true), data.justAddress, "Just Address: " + data.tag)
    }

}
