/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.3
import QtTest 1.0
import Ubuntu.Components 1.2
import "../../qml/Utils"

TestCase {
    name: "ViewStateTests"

    Item {
        id: root
        width: 0
        height: width
        ViewState {
            id: viewState
            anchors.fill: parent
            onStateChanged: console.log("STATE: ", state)
        }
    }
    SignalSpy {
        id: spy
        target: viewState
        signalName: "stateChanged"
    }

    function test_viewstate_state_change_data() {
        return [
                    {
                        tag: "Test small ff",
                        start: units.gu(20),
                        finish: units.gu(40),
                        startResult: "small",
                        endResult: "small",
                        shouldWait: false
                    },
                    {
                        tag: "Test small to medium",
                        start: units.gu(40),
                        finish: units.gu(100),
                        startResult: "small",
                        endResult: "medium",
                        shouldWait: true
                    },
                    {
                        tag: "Test small to large",
                        start: units.gu(40),
                        finish: units.gu(160),
                        startResult: "small",
                        endResult: "large",
                        shouldWait: true
                    },
                ]
    }

    function test_viewstate_state_change(data) {
        setWidth(data.start)
        verify(isState(data.startResult))
        spy.clear()
        setWidth(data.finish)
        if(data.shouldWait) {
            spy.wait(500)
            compare(spy.count, 1)
        }
        verify(isState(data.endResult))
    }

    function isState(state) {
        return viewState.state === state;
    }

    function setWidth(width) {
        root.width = width
    }
}
