/* Copyright (C) 2014-2015 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu Devices/

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QtGui/QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtQuickTest/quicktest.h>
#include <QFileInfo>
#include <QtCore>
#include <QtQml>
#include "app/DekkoGlobal.h"

QQuickView *qQuickViewer;

int main(int argc, char **argv)
{
    QGuiApplication a(argc, argv);

    DekkoGlobal::registerDekkoTypes("DekkoCore");
    DekkoGlobal::registerTrojitaCoreTypes("TrojitaCore");

    QStringList args = a.arguments();
    QString executable = args.at(0);

    QFileInfo info("./tests/qmltests");
    QString qrc = QString("qrc:///");
    char **s = (char**) malloc(sizeof(char*) * (10 + args.size() ) );
    int idx = 0;

    QByteArray srcdir = info.absoluteFilePath().toLocal8Bit();

    s[idx++] = executable.toLocal8Bit().data();
    s[idx++] = strdup("-import");
    s[idx++] = srcdir.data();
    s[idx++] = strdup("-import");
    s[idx++] = qrc.toLocal8Bit().data();

    for (int i = 1 ; i < args.size();i++) {
        s[idx++] = strdup(args.at(i).toLocal8Bit().data());
    }

    s[idx++] = 0;

    return quick_test_main( idx-1,s,"Dekko Qml Tests",srcdir.data());
}
