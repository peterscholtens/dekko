#ifndef TESTCONTACTSMODEL_H
#define TESTCONTACTSMODEL_H

#include <QTest>
#include "app/Contacts/ContactsModel.h"

namespace Dekko {
namespace Contacts {

class TestContactsModel : public QObject {
    Q_OBJECT
private:
    Dekko::Contacts::ContactsModel *m_contactsModel;

    void before();
    void after();
private slots:
    void testNewContact();
    void testRemoveStaleContactMoreThan();
};

}
}

#endif // TESTCONTACTSMODEL_H
